# -*- coding: utf-8 -*-
# Generated by Django 1.9.9 on 2019-03-08 07:19
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0004_myprofile'),
    ]

    operations = [
        migrations.AlterField(
            model_name='myprofile',
            name='attendence_monday',
            field=models.BooleanField(default=False, verbose_name='I plan to attend on Monday'),
        ),
        migrations.AlterField(
            model_name='myprofile',
            name='attendence_tuesday',
            field=models.BooleanField(default=False, verbose_name='I plan to attend on Tuesday'),
        ),
        migrations.AlterField(
            model_name='myprofile',
            name='attendence_wednesday',
            field=models.BooleanField(default=False, verbose_name='I plan to attend on Wednesday'),
        ),
        migrations.AlterField(
            model_name='myprofile',
            name='comments',
            field=models.TextField(blank=True, null=True, verbose_name='Comments'),
        ),
        migrations.AlterField(
            model_name='myprofile',
            name='meal_preference',
            field=models.CharField(choices=[('nopref', 'no preference'), ('vegetarian', 'vegetratian'), ('vegan', 'vegan')], max_length=128, verbose_name='Meal preference'),
        ),
        migrations.AlterField(
            model_name='myprofile',
            name='talk_title',
            field=models.CharField(blank=True, max_length=128, null=True, verbose_name='Title of your talk'),
        ),
    ]
