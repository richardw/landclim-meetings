import django
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "meetings.settings")
django.setup()

from events.models import MyProfile
from djqscsv import write_csv

registered_users = MyProfile.objects.filter(user__groups=3).all().values()

with open('registered_users_carbon-water-workshop.csv', 'wb') as csv_file:
    write_csv(registered_users, csv_file)