from django.db import models
from mezzanine.core import fields
from mezzanine.core.models import SiteRelated
from django.utils.translation import ugettext_lazy as _

class SitewideContent(SiteRelated):
    def logo_file_name():
        return '/'.join(['images'])
    site_logo = fields.FileField("Image", upload_to=logo_file_name, format="Image")
    #site_logo = models.ImageField(upload_to=logo_file_name, verbose_name="Image")
    class Meta:
        verbose_name = _('Sitewide Content')
        verbose_name_plural = _('Sitewide Content')
