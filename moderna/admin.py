from django.contrib import admin
from mezzanine.utils.admin import SingletonAdmin
from moderna.models import SitewideContent

admin.site.register(SitewideContent, SingletonAdmin)
